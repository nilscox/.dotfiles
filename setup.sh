#!/bin/sh

GIT_DIRCOLORS='git://github.com/seebi/dircolors-solarized'
GIT_OH_MY_ZSH='git://github.com/robbyrussell/oh-my-zsh.git'
GIT_VUNDLE_VIM='git://github.com/VundleVim/Vundle.vim.git'
GIT_ZSH_GIT_PROMPT='git://github.com/olivierverdier/zsh-git-prompt'
GIT_NVM='git://github.com/creationix/nvm.git'
GIT_I3BLOCKS_CONTRIB='git://github.com/vivien/i3blocks-contrib.git'

[ -z "$DOT" ] && DOT=$(dirname $(readlink -f "$0"))
[ -z "$DEST" ] && DEST="$HOME"

clone_or_pull() {
  if [ -d "$2" ]; then
    (cd "$2" && git pull)
  else
    git clone --depth=1 "$1" "$2"
  fi
}

# zsh
clone_or_pull "$GIT_OH_MY_ZSH" "$DEST/.oh-my-zsh"
ln -sf "$DOT/zshrc" "$DEST/.zshrc"
ln -sf "$DOT/oh-my-zshrc" "$DEST/.oh-my-zshrc"
if [ "$DISTRIB" = "arch" -o "$DISTRIB" = "archarm" ]; then
  ln -sf "$DOT/zshrc.archlinux" "$DEST/.zshrc.archlinux"
elif [ "$DISTRIB" = "debian" ]; then
  ln -sf "$DOT/zshrc.debian" "$DEST/.zshrc.debian"
fi
mkdir -p "$DEST/.config"
clone_or_pull "$GIT_DIRCOLORS" "$DEST/.config/dircolors-solarized"
ln -sf "$DEST/.config/dircolors-solarized/dircolors.ansi-universal" "$DEST/.dir_colors"
clone_or_pull "$GIT_ZSH_GIT_PROMPT" "$DEST/.config/zsh-git-prompt"
chsh -s /bin/zsh

# home dirs
mkdir ~/pcloud ~/twix

# default browser
xdg-settings set default-web-browser firefox.desktop

# git
ln -sf "$DOT/gitconfig" "$DEST/.gitconfig"
ln -sf "$DOT/gitignore_global" "$DEST/.gitignore_global"

# ssh
ln -sf "$DOT/ssh.config" "$DEST/.ssh/config"

# nvm
clone_or_pull "$GIT_NVM" "$DEST/.nvm"

# i3
ln -sf "$DOT/i3" "$DEST/.config/i3"
ln -sf "$DOT/i3blocks" "$DEST/.config/i3blocks"
clone_or_pull "$GIT_I3BLOCKS_CONTRIB" "$DEST/.config/i3blocks-contrib"
ln -sf "$DEST/.config/i3blocks-contrib/batterybar/batterybar" "$DEST/.config/i3blocks/blocklets"
ln -sf "$DEST/.config/i3blocks-contrib/cpu_usage/cpu_usage" "$DEST/.config/i3blocks/blocklets"
ln -sf "$DEST/.config/i3blocks-contrib/iface/iface" "$DEST/.config/i3blocks/blocklets"
ln -sf "$DEST/.config/i3blocks-contrib/mediaplayer/mediaplayer" "$DEST/.config/i3blocks/blocklets"
ln -sf "$DEST/.config/i3blocks-contrib/memory/memory" "$DEST/.config/i3blocks/blocklets"
ln -sf "$DEST/.config/i3blocks-contrib/volume/volume" "$DEST/.config/i3blocks/blocklets"

# terminator
mkdir -p "$DEST/.config/terminator"
ln -sf "$DOT/terminator.config" "$DEST/.config/terminator/config"
ln -sf "$DOT/gtk.css" "$DEST/.config/gtk-3.0/gtk.css"

# imwheel
ln -sf "$DOT/imwheelrc" "$DEST/.imwheelrc"

# vim
clone_or_pull "$GIT_VUNDLE_VIM" "$DEST/.vim/bundle/Vundle.vim"
ln -sf "$DOT/vimrc" "$DEST/.vimrc"
vim +PluginInstall +qall

# sublime text
PACKAGE_DIR="$DEST/.config/sublime-text-3/Packages"
USER_DIR="$PACKAGE_DIR/User"
mkdir -p "$PACKAGE_DIR"
rm -rf "$USER_DIR"
ln -s "$DOT/sublime" "$USER_DIR"
rm -f "$DEST/.sublime"
ln -s "$USER_DIR" "$DEST/.sublime"

# vscode
ln -sf "$DOT/vscode/settings.json" "$DEST/.config/Code\ -\ OSS/User"
ln -sf "$DOT/vscode/keybindings.json" "$DEST/.config/Code\ -\ OSS/User"
extensions=(
  anjali.clipboard-history
  azemoh.one-monokai
  davidanson.vscode-markdownlint
  dbaeumer.vscode-eslint
  donjayamanne.githistory
  esbenp.prettier-vscode
  jpoissonnier.vscode-styled-components
  ms-azuretools.vscode-docker
  msjsdiag.debugger-for-chrome
  ms-vscode.vscode-typescript-tslint-plugin
  pkief.material-icon-theme
  sashaweiss.block-travel
  sleistner.vscode-fileutils
  visualstudioexptteam.vscodeintellicode
)
for ext in "${extensions[@]}"; do code --install-extension "$ext"; done
