# 3rd party
# =========

[ -f ~/.oh-my-zshrc ] && source ~/.oh-my-zshrc
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# History
# =======

HISTFILE=~/.zsh_history
HISTSIZE=42000
SAVEHIST=42000

setopt append_history           # append
setopt extended_history         # store command timestamp and duration
setopt hist_ignore_all_dups     # no duplicate
setopt hist_reduce_blanks       # trim blanks
setopt hist_verify              # show before executing history commands
setopt inc_append_history       # add commands as they are typed, dont wait until shell exit
setopt share_history            # share hist between sessions
setopt bang_hist                # !keyword
unsetopt hist_ignore_space      # ignore space prefixed commands

# Shell options
# =============

setopt auto_cd                  # if command is a path, cd into it
setopt auto_remove_slash        # self explicit
setopt chase_links              # resolve symlinks
setopt extended_glob            # activate complex pattern globbing
setopt dvorak                   # for dvorak keymap
setopt prompt_subst             # enable prompt command substitution
unsetopt glob_dots              # dont include dotfiles in globbing
unsetopt beep                   # no bell on error
unsetopt bg_nice                # no lower prio for background jobs
unsetopt hist_beep              # no bell on error in history
unsetopt list_beep              # no bell on ambiguous completion
unsetopt rm_star_silent         # ask for confirmation for `rm *'

# Bind keys
# =========

bindkey ';5C' emacs-forward-word
bindkey ';5D' emacs-backward-word

# Exported varialbes
# ==================

export TERM='xterm-256color'
export PAGER='most'
export PATH="$PATH:$HOME/.local/bin"
export EDITOR='vim'

# Colors
# ======

autoload -U colors && colors
eval `dircolors ~/.dir_colors`

# Completion
# ==========

setopt hash_list_all            # hash everything before completion
setopt always_to_end            # when completing from the middle of a word, move the cursor to the end of the word
setopt complete_in_word         # allow completion from within a word/phrase
setopt correct                  # spelling correction for commands
setopt correct_all              # try to correct spelling of commands
setopt auto_list                # automatically list choices on ambiguous completion
setopt list_packed              # pack columns
unsetopt list_ambiguous         # show completion when is command ambiguous
unsetopt menu_complete          # do not autoselect the first completion entry.
unsetopt complete_aliases       # complete alisases

zmodload -i zsh/complist

# auto rehash commands
zstyle ':completion:*' rehash true

# for all completions: menuselection
zstyle ':completion:*' menu select=1

# for all completions: grouping the output
zstyle ':completion:*' group-name ''

# for all completions: color
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# completion of .. directories
zstyle ':completion:*' special-dirs true

# for all completions: show comments when present
zstyle ':completion:*' verbose yes

# fault tolerance
zstyle ':completion:*' completer _complete _correct _approximate
# (1 error on 3 characters)
zstyle -e ':completion:*:approximate:*' max-errors 'reply=( $(( ($#PREFIX+$#SUFFIX)/3 )) numeric )'

# case insensitivity
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

# for all completions: grouping / headline / ...
zstyle ':completion:*:messages' format $'\e[01;35m -- %d -- \e[00;00m'
zstyle ':completion:*:warnings' format $'\e[01;31m -- No Matches Found -- \e[00;00m'
zstyle ':completion:*:descriptions' format $'\e[01;33m -- %d -- \e[00;00m'
zstyle ':completion:*:corrections' format $'\e[01;33m -- %d -- \e[00;00m'

# statusline for many hits
zstyle ':completion:*:default' select-prompt $'\e[01;35m -- Match %M    %P -- \e[00;00m'

# use cache
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zsh

zstyle ':completion:*:manuals' separate-sections true

# 2x control is completion from history!!!
zle -C hist-complete complete-word _generic
zstyle ':completion:hist-complete:*' completer _history

# ~dirs: reorder output sorting: named dirs over userdirs
zstyle ':completion::*:-tilde-:*:*' group-order named-directories users

# ssh: reorder output sorting: user over hosts
zstyle ':completion::*:ssh:*:*' tag-order "users hosts"

# kill: advanced kill completion
zstyle ':completion::*:kill:*:*' command 'ps xf -U $USER -o pid,%cpu,cmd'
zstyle ':completion::*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;32'

# rm: advanced completion (e.g. bak files first)
zstyle ':completion::*:rm:*:*' file-patterns '*.o:object-files:object\ file *(~|.(old|bak|BAK)):backup-files:backup\ files *~*(~|.(o|old|bak|BAK)):all-files:all\ files'

autoload -U compinit && compinit
autoload -U select-word-style && select-word-style bash

# Aliases
# =======

alias ls='ls --color=auto'
alias la='ls -A'
alias ll='ls -hHl --file-type'
alias lla='ll -A'
alias l='ll'
alias e='echo'
alias m='man'
alias g='git'
alias f='feh --scale-down'
alias v='vim'
alias d='docker'
alias dcp='docker-compose'
alias s='sudo '
alias p='python'
alias a='atom'
alias hl='highlight'

alias ...='../..'
alias ....='../../..'

alias -s pdf="evince "
alias -s jpg="feh "
alias -s jpeg="feh "
alias -s bmp="feh "
alias -s png="feh "
alias -s tiff="feh "
alias -s avi="vlc "
alias -s mpg="vlc "
alias -s mpeg="vlc "
alias -s jar="java -jar "

alias -g G="| grep"
alias -g L="| less"
alias -g M=' | most'
alias -g H=' | head'
alias -g T=' | tail'
alias -g TF=' | tail -f'
alias -g NUL=' > /dev/null 2>&1'
alias -g C=' | xclip -selection clipboard'
alias -g ST='| st -'
alias -g HL='| highlight'

alias -g P1=$'| awk \'{ print $1 }\''
alias -g P2=$'| awk \'{ print $2 }\''
alias -g P3=$'| awk \'{ print $3 }\''
alias -g P4=$'| awk \'{ print $4 }\''
alias -g P5=$'| awk \'{ print $5 }\''

# Functions
# =========

killport() {
  if [ -z "$1" ]; then
    echo "usage: $0 <port>" 2>&1
    return 1
  fi

  pid=$(sudo lsof -i ":$1" | grep '(LISTEN)' | tail -1 | awk '{ print $2 }')

  if [ -z "$pid" ]; then
    return 1;
  fi

  kill "$pid"
}

# Prompt
# =====

prompt_user="%{$fg_no_bold[cyan]%}%n%{$reset_color%}"
prompt_host="%{$fg_bold[black]%}%m%{$reset_color%}"
prompt_dir="%{$fg_no_bold[yellow]%}%2~%{$reset_color%}"
prompt_sudo="%#"

prompt_date() {
  echo "%{$fg_bold[black]%}[%{$fg_bold[yellow]%}$(date '+%H:%M:%S')%{$fg_bold[black]%}]%{$reset_color%}"
}

prompt_status() {
  last_status="$?"

  if [[ "$last_status" -ne 0 ]]; then
    echo "%{$fg_bold[black]%}[%{$fg_no_bold[red]%}$last_status%{$fg_bold[black]%}]%{$reset_color%} "
  fi
}

if [[ -n "$SSH_CLIENT" ]]; then
  prompt_host="%{$fg_bold[green]%}%m%{$reset_color%}"
fi

if [[ $UID == 0 ]]; then
  prompt_user="%{${fg_bold[red]%}%n%{$reset_color%}"
fi

source ~/.config/zsh-git-prompt/zshrc.sh

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git%{$reset_color%}:("
ZSH_THEME_GIT_PROMPT_SUFFIX=")\n​"

PROMPT='$(prompt_status)$(git_super_status)${prompt_user}:${prompt_host} ${prompt_dir} ${prompt_sudo} '
RPROMPT='$(prompt_date)'

# Custom config
# =============

for file in ~/.zshrc.*; do
  source "$file"
done
