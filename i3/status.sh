#!/bin/sh

# The Sway configuration file in ~/.config/sway/config calls this script.

# emojis

# speaker low `🔈`
# speaker medium `🔉`
# speaker high `🔊`
# speaker muted `🔇`
# battery `🔋`
# electric plug `🔌`
# check mark `✔️`
# cross mark `❌`
# stopwatch `⏱️`

# audio

volume=$(pamixer --get-volume)
volume_human=$(pamixer --get-volume-human)

if [ "$volume_human" = "muted" ]; then
  out_audio="🔇"
elif [ "$volume" -lt "10" ]; then
  out_audio="🔈 $volume_human"
elif [ "$volume" -lt "90" ]; then
  out_audio="🔉 $volume_human"
else
  out_audio="🔊 $volume_human"
fi

# internet access

local_ip=$(ip addr | grep 'wlp' | grep 'global' | awk '{ print $2 }' | sed 's|/.*||')

if [ -z "$local_ip" ]; then
  out_internet="❌ 127.0.0.1"
else
  out_internet="✔️ $local_ip"
fi

# battery level

battery_device=$(upower --enumerate | grep 'BAT')
battery_status=$(upower --show-info "$battery_device" | grep "state" | awk '{ print $2 }')
battery_percentage=$(upower --show-info "$battery_device" | grep "percentage" | awk '{ print $2 }')
battery_tte=$(upower --show-info "$battery_device" | grep "time to empty" | awk '{ print $4 " " $5 }')
battery_level=$(echo "$battery_percentage" | sed 's/%$//')

if [ "$battery_status" = "discharging" ]; then
  out_battery="🔋 $battery_percentage ($battery_tte)"
  if [ "$battery_level" -lt "10" ]; then
    if (( $(date +'%s') % 2 == 0 )); then
      out_battery="$out_battery ❌"
    else
      out_battery="$out_battery   "
    fi
  fi
else
  out_battery="🔌 $battery_percentage"
fi

# date

out_date="⏱️ $(date "+%a %F %H:%M:%S")"

# output

echo "$out_audio | $out_internet | $out_battery | $out_date"
