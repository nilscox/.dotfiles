" Global config
" =============

set nocompatible
set expandtab
set softtabstop=2
set shiftwidth=2
set laststatus=2
set encoding=utf-8
set colorcolumn=80

syntax on
filetype off

" Vundle plugins manager
" ======================

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'scrooloose/syntastic'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'Lokaltog/vim-powerline'

call vundle#end()

" Plugins configuration

filetype plugin indent on

let g:vim_markdown_folding_disabled=1

let g:ctrlp_map = '<Leader>t'
let g:ctrlp_match_window_bottom = 0
let g:ctrlp_match_window_reversed = 0
let g:ctrlp_custom_ignore = '\v\~$|\.(o|swp|pyc)$|(^|[/\\])\.(hg|git|bzr)($|[/\\])'
let g:ctrlp_working_path_mode = 0
let g:ctrlp_dotfiles = 0
let g:ctrlp_switch_buffer = 0

" Custom mappings

noremap T 8<Down>
noremap N 8<Up>

noremap l n
noremap L N

nmap ; :CtrlPBuffer<CR>
" nmap :# :NERDTreeToggle<CR>

nmap <C-L> :set invnumber<CR>:set invrelativenumber<CR>

" Other
" =====

if $TERM == "xterm-256color" || $TERM == "screen-256color" || $TERM == "rxvt-unicode-256color"
  set t_Co=256
endif

highlight LineNr ctermfg=DarkGrey ctermbg=NONE
set listchars=tab:>-,trail:-
set list

" Setup
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
" vim +PluginInstall +qall
